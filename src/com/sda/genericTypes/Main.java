package com.sda.genericTypes;

public class Main {
    public static void main(String[] args) {
        GenericBox<String> stringBox = new GenericBox<String>("JavaRemoteRO36");
        System.out.println(stringBox.getItem());


        GenericBox<Integer> integerBox = new GenericBox<Integer>(36);
        System.out.println(integerBox.getItem());


        Samsung s22 = new Samsung("Samsung", "s22", "gray");
        GenericBox<Samsung> samsungBox = new GenericBox<Samsung>(s22);
        samsungBox.getItem().printPhoneDetails();

        GenericBoxLimited<Samsung> samsungGenericBoxLimited = new GenericBoxLimited<Samsung>(s22);
        samsungGenericBoxLimited.getItem().printPhoneDetails();
        samsungGenericBoxLimited.print();
        GenericBoxLimited<Phone> phoneGenericBoxLimited = new GenericBoxLimited<>(null);

    }
}
