package com.sda.genericTypes;

// O cutie pentru class String
public class GenericBox<T> {
    private T item;

    public GenericBox(T item) {
        this.item = item;
    }

    public T getItem() {
        return this.item;
    }
}
