package com.sda.polymorphism;

public class Parent {

    private String name;

    public Parent(String name) {
        this.name = name;
    }

    public void showName() {
        System.out.println("Parent: " + this.name);
    }

    public String getName() {
        return this.name;
    }

}
