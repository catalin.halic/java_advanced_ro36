package com.sda.polymorphism;

public class Main {
    public static void main(String[] args) {
       // polymorphism => mai multe forme

       // 1. overriding
//        - suprascriem o metoda din clasa parinte  in clasa copil

        Parent parent = new Parent("Ion");
        parent.showName();

        System.out.println("-----------");

        Child child = new Child("Mihai");
        child.showName();

        // 2. overload
//        overload reprezinta supraincarcarea unui constructor sau unei metode.
//        se tine cont doar de semnatura metode.

//        3. Polimorfism
        Parent p = new Child("Catalin");

        System.out.println("child name: " + p.getName());


        Object c = new Child("Raul");
        if(c instanceof Parent) {
            System.out.println("Variabila c este de tipul Parent");
        } else {
            System.out.println("Variabila c NU este de tipul Parent");

        }

        if(parent instanceof  Child) {
            System.out.println("parent este instanta lui child");
        }


//         instanceOf
//        int [] names = {3, 4, 5 };
//        if(names instanceof Object) {
//            System.out.println("is object");
//        }

//        NU SE POATE, pentru ca partea stanga stie mai multe decat in dreapta
//        Child cx = new Parent("test");



    }

    // EXEMPLU OVERLOAD METODE
    public void printMessage() {}

//    nu se considera overload
//    public int printMessage() {}

    public void printMessage(String message) {}

    public void printMessage(String message, int type) {}

    public void printMessage(int type, String message) {}
}
