package com.sda.collections;

import com.sda.genericTypes.Samsung;

import java.util.*;

import static java.lang.System.exit;

public class Main {
    public static void main(String[] args) {
//        learnList();
//        learnSet();
//        learnMap();
//        mapWithListValue();
//        learnQueue();
//        exLists();
        exSet();
    }

    public static void learnList() {
        List<String> nameList = new ArrayList<>();

//        List<String nameList = new LinkedList<>();

        // pentru a adauga un element in lista
        nameList.add("Ana");
        nameList.add("Maria");
        nameList.add(2,"Andrei");

        // afisare elemente din lista
        for(String name : nameList) {
            System.out.println(name);
        }

        // dimnesiunea unei liste
        System.out.println("Dimensiune Lista: " + nameList.size());

        // pentru a accesa ultimul element din lista folosim size() - 1, echivalentul lui length - 1

        // stergerea unui element
        nameList.remove("Ana"); // pentru obiectele diferite de String, in clasa obiectului trebuie sa implementam metoda equals()
        nameList.remove(nameList.size() - 1); // sterge ultimul element din lista

//        sterge mai multe elemente din lista pe care noi o dam
//        nameList.removeAll()
        // [Ana, Maria]

        for (String name : nameList) {
            System.out.println(name);
        }

        nameList.add("Victor");
        nameList.add("Mihai");
        nameList.add("Ion");

//        nameList.addAll() // adauga o lista acestei liste (concatenare de liste)

//        for (String name : nameList) {
//            System.out.println(name);
//        }

        // accesare unui element din lista
        for(int i = 0; i < nameList.size(); i++) {
            System.out.println(nameList.get(i));
        }

        String result = nameList.get(nameList.size() - 1);

        System.out.println("ultimul element este: "  + result);

        // stergerea elementelor din lista
//        nameList.clear();


        // daca sunt egale => compare => 0
//        data primul este mai mare ca al doilea returneaza 1
//        daca al doile este mai mare a primul returneaza -1
        System.out.println("Sortare lista");
//        nameList.sort(new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o2.compareTo(o1);
//            }
//        });

//        nameList.sort((String str1, String str2) -> str1.compareTo(str2));

        for(int i = 0; i < nameList.size(); i++) {
            System.out.println(nameList.get(i));
        }

    }

    public static void learnSet() {
        Set<String> nameSet = new HashSet<>();

        // adaugarea elementelor in set
        nameSet.add("Ana"); // HashCode: 34
        nameSet.add("Mihai"); //HashCode: 102
        nameSet.add("B"); // HashCode: 2
        nameSet.add("In"); // HashCode: 5
        nameSet.add(null);

        // pentru sterge un element din set
//        nameSet.remove("Ana");

        // afisarea elementelor din set
        for(String name : nameSet) {
            System.out.println(name);
        }

        System.out.println("Afisare folosind metoda toArray()");
        for (int i = 0; i < nameSet.size(); i++) {
            System.out.println(nameSet.toArray()[i]);
        }

        // stergerea elementelor
        nameSet.clear();
    }

    public static void learnMap() {
        // Map => un dictionar
        // cuvant => definitie
        // key (K) => value (V)

        Map<String, String> nameMap = new HashMap<>();

        // adaugarea elementelor in map
        nameMap.put("0234", "Andrei");
        nameMap.put("1222", "Ion");
        nameMap.put("2222", "Mihai");
        nameMap.put("3434", "Mircea");
        nameMap.put("0000", "Dan");
        nameMap.put("0909", "Ilie");

        // afisarea elemetelor
        // accesarea elementelor din map se face pe baza lui key => folosind metoda get, adica nameMap.get("1234")
        for(String key : nameMap.keySet()) {
            System.out.println("key: " + key + " value: " + nameMap.get(key));
        }

        // accesarea elementelor din map
        String value = nameMap.get("0000");
        System.out.println("Value for key 0000 is " + value);

        // editarea unei key
        // verificam daca key-ul este in map
        if(nameMap.containsKey("0000")) {
            // salvam valoarea pentru acest key intr-un string
            String valueDan = nameMap.get("0000");

            // verificam daca noul key este prezent in map
            if(nameMap.containsKey("0001")) {
                System.out.println("Nu putem actualiza key-ul pentru ca este deja prezent!");
            } else {
                // stergem inregistreea cu keyul 0000
                nameMap.remove("0000");

                // adaugam noul key cu valoare precedenta
                nameMap.put("0001", valueDan);
            }
        }

        // editarea unei valori dintr-un key
        if(nameMap.containsKey("2222")) {
            String valueMihai = nameMap.get("2222");

            valueMihai += " Ion";

            // daca adaugam un element in map care deja exista, atunci vom suprascrie valoarea key-ul deja existent
            nameMap.put("2222", valueMihai);
        } else {
            nameMap.put("2222", "Mihai Ion");
        }

        for(String key : nameMap.keySet()) {
            System.out.println("key: " + key + " value: " + nameMap.get(key));
        }

        // afisam lungimea unui map
        System.out.println("Map size: " + nameMap.size());

        // sterge elementele din map
//        nameMap.clear();

//        nameMap.keySet().size();

//        ASEMENEA UNUI JSON (JSON => un mod de comunicare pe internet intre 2 aplicatii si este sub forma de String")
//        {
//            "0000": "Andrei",
//            "1234": "Mihai"
//        }
    }

    public static void mapWithListValue(){
        //    K                V
        Map<String, List<Person>> personMap = new HashMap<>();

        List<Person> personList = new ArrayList();
        Person p1 = new Person("Ion");
        Person p2 = new Person("Mihai");
        Person p3 = new Person("Ana");

        personList.add(p1);
        personList.add(p2);
        personList.add(p3);

        personMap.put("list1", personList);

        // parcurgem mapul care este key = String, value = List<Person>
        for(String key : personMap.keySet()) {

            // pentru fiecare key din map, salvam value (lista de Person)
            List<Person> value  = personMap.get(key);

            // afisam key
            System.out.println(key);

            // parcurgem lista de Person salvata in variabila "value"
            // care reprezinta valuarea din map pentru key
            for(Person p : value) {
                // afisam numele fiecare persoane din lista
                System.out.println("-- " + p.getName());
            }
        }
    }

    public static void learnQueue() {
        //FIFO => First In First Out

        Queue<String> stringQueue = new PriorityQueue<>(3, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });

        // adaugarea unui element in queue
        stringQueue.add("Remote");
        stringQueue.add("SDA");
        stringQueue.add("Java");
        stringQueue.add("Test");

        for(String item : stringQueue) {
            System.out.println(item);
        }

        // stergerea unui element din queue
        stringQueue.remove(); // sterge primul element din lista

        stringQueue.remove("Java"); // sterge elementul

        // dimensiunea lui queue (lungimea)
        System.out.println("Dimensiune: " + stringQueue.size());

        for(String item : stringQueue) {
            System.out.println(item);
        }

        // peek() => returneaza primul element din queue
        String resultPeek = stringQueue.peek();
        System.out.println("peek(): " + resultPeek);
        System.out.println("Dimensiune: " + stringQueue.size());

        // poll() => returneaza primul element din queue, apoi il sterge
        String resultPoll = stringQueue.poll();
        System.out.println("poll(): " + resultPoll);
        System.out.println("Dimensiune: " + stringQueue.size());

        for(String item : stringQueue) {
            System.out.println(item);
        }
    }

    public static void exLists() {
        /**
         * Create a List and display its result (data should be provided by the user - console):
         * a) Purchases to be made. *If an element already exists on the list, then it should not be added.
         * b) Add to the example above the possibility of “deleting” purchased elements
         * c) Display only those purchases that start with „m” (e.g. milk)
         * d) * View only purchases whose next product on the list starts with „m” (e.g. eggs, if milk was next on the list)
         */

        Scanner scanner = new Scanner(System.in);

        List<String> items = new ArrayList<>();

        while(scanner.hasNext()) {
            String input = scanner.nextLine();

            if(input.equalsIgnoreCase("exit")) {
                break;
            }

            // input.toLowerCase() transforma MERe => mere
            if(!items.contains(input.toLowerCase())) {

                // adauga elementul in lista cu toLowerCase() adica cu litere mici
                items.add(input.toLowerCase());
            } else {
                System.out.println("Do you want to remove " + input + "? (Y/n)");

                String response = scanner.nextLine();

                if(response.equalsIgnoreCase("y")) {
                    items.remove(input.toLowerCase());
                } else {
                    System.out.println("Elementul nu a fost sters");
                }
            }

            System.out.println("-------");
            for(String item : items) {
                if(item.startsWith("m")) {
                    System.out.println(item);
                }
            }
        }






    }

    public static void exSet() {
        /**
         * 1. Create a set consisting of colors - given from the user.
         * 2. Present the removal of individual elements from the set.
         * 3. Display the collection before and after sorting.
         */

        Scanner scanner = new Scanner(System.in);

        Set<String> colorsSet = new HashSet<>();

         while(scanner.hasNext()) {
             String color = scanner.nextLine();

             if(color.equalsIgnoreCase("exit")) {
                 break;
             }

             colorsSet.add(color);


             System.out.println("------");
             for(String c : colorsSet) {
                 System.out.println(c);
             }
         }

         while(true) {
             System.out.println("------");
             for(String c : colorsSet) {
                 System.out.println(c);
             }

             String colorToRemove = scanner.nextLine();

             if(colorToRemove.equalsIgnoreCase("exit")) {
                 break;
             }

             if(colorsSet.contains(colorToRemove)) {
                 colorsSet.remove(colorToRemove);
             }
         }

        System.out.println("Set before sort");
         for (String c : colorsSet) {
             System.out.println(c);
         }

         // how to sort a set?
        // clasa ArrayList poate primi un Set ca si parametru in constructor, pe baza caruia il transforma in List
        List<String> colorList = new ArrayList<>(colorsSet);

        // red
        // blue
        // green
        // yellow

        colorList.sort((color1, color2) -> color1.compareTo(color2));

        System.out.println("Set after sort");
        for (String c : colorList) {
            System.out.println(c);
        }

    }
}
