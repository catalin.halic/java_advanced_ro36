package com.sda.threads;

public class StopWatchThread extends Thread {
    private String name;

    public StopWatchThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 100; i++) {
                System.out.println(this.name + " i: " + i);

//                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
