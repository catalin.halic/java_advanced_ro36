package com.sda.threads;

public class Main {
    public static void main(String[] args) {
        // Threads
        // 1. Runnable Interface
        // 2. Thread Class

//       sleepThread();

        // extindem clasa THREAD iar StopWatchThread este copilul clasei
//        StopWatchThread t1 = new StopWatchThread("t1");
//        t1.start();
//
//        StopWatchThread t2 = new StopWatchThread("t2");
//        t2.start();
//
//        // pentru  a creeaa un obiect de tipul thread avem nevoie de clasa thread
//        StopWatchRunnable r1 = new StopWatchRunnable();
//        Thread threadRunnable = new Thread(r1);
//        threadRunnable.start();
//
//        // pentru a porni un Thread, avem nevoie de clasa Thread
//
//        Thread t3 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        });
//
//        for(int i = 0; i < 10; i++) {
//            System.out.println("Main thread: " + i);
//        }


        Bench bench = new Bench(1);
        SeatTakerThread seatTakerThread = new SeatTakerThread(bench);
        SeatTakerThread seatTakerThread2 = new SeatTakerThread(bench);

        seatTakerThread.start();
        seatTakerThread2.start();
    }

    public static void sleepThread() {
        try {
            System.out.println("Bun venit!");
            // 1000  milisecunde => 1 secunda
            // sleep() => Pune threadul pe pauza timp de x milisecunde
            Thread.sleep(1000 * 5);
            System.out.println("Continuam...");
            Thread.sleep(1000 * 5);
            System.out.println("O zi buna!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
