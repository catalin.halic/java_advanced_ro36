package com.sda.inheritance;

public class Bike extends Vehicle {
    private double weight;

    public Bike(double weight) {
//        super(); // - apelam constructorul default din clasa Vehicle
        super(2, "black", 40); // initializam proprietatile din clasa parinte cu valori predefinite

        this.weight = weight;


    }
}
