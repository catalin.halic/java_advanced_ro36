package com.sda.inheritance;

class Main {
    public static void main(String[] args) {
        // 1. mostenirea se realizeaza cu ajutorul cuvantului "extends";
        // 2. o clasa poate sa mosteneasca o alta clasa;
        // 3. o clasa are UN SINGUR Parinte;
        // 4. un Parinte are o infinitate de copii;
        // 5. cand mostenim o clasa, preluam in copil toate proprietatile, metodele si toti constructorii
        // din clasa parinte


        Car dacia = new Car("Dacia", "1300", true, 4, "Albastru", 160);
//        - un obiect de tipul Vehicul
//        - si obiectul de tipul Car

        System.out.println("Max speed: " + dacia.getMaxSpeed());
//        System.out.println("Color: " + dacia.);

        Bike bike = new Bike(30.0);
        bike.setWheelsNumber(4);





    }
}
