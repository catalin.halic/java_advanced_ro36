package com.sda.inheritance;

public class Car extends Vehicle {
    private String brand;
    private String model;
    private boolean isManual;

    public Car(String brand, String model, boolean isManual, int wheelsNumber, String color, double maxSpeed) {
        // super - este echivalentul lui "this" cu mentiunea ca se foloseste doar in relatii de mostenire
        // super - face referire la proprietati, metode, constructori din clasa PARINTE

        // trimite parametrii catre constructorul din clasa parinte Vehicle
        // in acest caz, super() trebuie sa fie prima line din Constructor!!!!
        // in acest caz super() creeaza o instanta de tipul Vehicle adica este echivalentul lui new Vehicle(wheelsNumber, color, maxSpeed)
        super(wheelsNumber, color, maxSpeed);

        this.brand = brand;
        this.model = model;
        this.isManual = isManual;
    }

}
