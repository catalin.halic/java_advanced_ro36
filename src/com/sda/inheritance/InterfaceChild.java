package com.sda.inheritance;

public interface InterfaceChild extends InterfaceParent {
    void showMessage();

    @Override
    default void printMessage() {
        System.out.println("message");
    }
}
