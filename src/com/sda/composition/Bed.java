package com.sda.composition;

 class Bed {
    private int length;
    private int width;

    public Bed(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public void makeBed() {
        System.out.println("Make bed in progress...");
    }

    @Override
    public String toString() {
        return "Bed: lungime: " + this.length + " mm, latime: " + this.width + " mm.";
    }
}
