package com.sda.composition;

public class TvSamsung {
    private int size;
    private String color;
    private boolean isSmart;

    public TvSamsung(int size, String color, boolean isSmart) {
        this.size = size;
        this.color = color;
        this.isSmart = isSmart;
    }

    public int getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public boolean isSmart() {
        return isSmart;
    }

    @Override
    public String toString() {
        return "TV Samsung: " + "diagonala: " + this.size + " cm, color: " + this.color + ", is smart: " + this.isSmart;
    }

    @Override
    public boolean equals(Object object) {
        // Object (obiectul parametru) => Tipul SamsungTV => size = 12, color = red, isSmart = true;
        // this class (obiectul din clasa) => size = 12, color = red, isSmart = true;

//        if(object.isSmart == this.isSmart && object.color == this.color && object.size == this.size) {
//            return true;
//        }
//
//        return false;

        String obj = object.toString();  // "TV Samsung: " + "diagonala: " + 12 + " cm, color: " + red + ", is smart: " + true;
        String tvSamsung = this.toString(); // // "TV Samsung: " + "diagonala: " + 12 + " cm, color: " + red + ", is smart: " + true;

        return obj.equals(tvSamsung);

    }
}
