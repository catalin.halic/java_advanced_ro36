package com.sda.composition;

public class Main {
    public static void main(String[] args) {
        // Compozitia este principiul care ne permite sa avem proprietati pentru un obiect de tipul altor obiecte

        Bed bed = new Bed(2000, 1800);
        TvSamsung tvSamsung = new TvSamsung(165, "black", true);

        Bedroom bedroom = new Bedroom();
        bedroom.setBed(bed);
        bedroom.setTvSamsung(tvSamsung);

        System.out.println(bedroom);

        String abc = "Ana" + "are" + "mere" + "rosii";
        abc = "Ana are" + "mere" + "rosii";
        abc = "Ana are mere" + "rosii";
        abc = "Ana are mere rosii";

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ana").append("are").append("mere").append("rosii");
    }
}
