package com.sda.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<String> nameList = new ArrayList<>();
        nameList.add("Ion");
        nameList.add("Mihai");
        nameList.add("Stefan");
        nameList.add("Andrei");
        nameList.add("Mircea");

        // collect -> ne ajuta sa transformam streamul inapoi intr-o colectie (List, Set)
        List<String> newValues =  nameList.stream().collect(Collectors.toList());

        // findFirst() => ne ajuta sa gasim primul element din stream
        // findFirst() => returneaza un Optional cu primul element din stream
        Optional<String> firstElement = nameList.stream().findFirst();

        firstElement.ifPresentOrElse(
                (String element) -> System.out.println("Primul element este: " + element),
                () -> System.out.println("Nu avem elemente"));

        // findAny() => ne ajuta sa gasim cel putin un element
        Optional<String> anyElement = nameList.stream().findAny();
        anyElement.ifPresentOrElse(
                (String element) -> System.out.println("Elementul este: " + element),
                () -> System.out.println("Nu avem elemente"));


        // filter() => ne ajuta sa filtram rezultatele (adica sa cautam intr-o colectie

        System.out.println("Varianta foreach si if");
        // varianta OLD SCHOOL
        List<String> nameStartWithM = new ArrayList<>();
        for(String name : nameList) {
            if(name.startsWith("M")) {
                nameStartWithM.add(name);
            }
        }

        for(String result : nameStartWithM) {
            System.out.println(result);
        }

        System.out.println("Varianta cu Stream + fitler");
        // varianta cu stream si filter
        List<String> results = nameList
                .stream() // transforma lista dintr-un element in mai multe elemente mai mici
                .filter((name) -> name.startsWith("M"))  // pastreaza elementele care indeplinesc conditia
                .collect(Collectors.toList()); // transforma streamul inapoi in lista

        results.stream().forEach((result) -> System.out.println(result));

        System.out.println("Metoda Map");

        // map() => ne ajuta sa prelucram elementele din stream si sa formam alte elemente
        List<Integer> numbers = nameList
                .stream()
                .filter((name) -> name.startsWith("M"))
                .map((name) -> name.length()) // ne ajuta sa modificam streamul si sa obtinem alte obiecte
                .filter((number) -> number > 7)
                .collect(Collectors.toList());

        numbers.stream().forEach((number) -> System.out.println(number));

        // allMatch() => returneaza true sau false daca TOATE elemente indeplinesc conditia sau conditile
        boolean allNamesBiggerThan2 = nameList
                .stream()
                .allMatch((name) -> name.length() > 2);
        System.out.println("AllMath() -> All names bigger than 2: " + allNamesBiggerThan2);

        List<Person> personList = new ArrayList<>();
        Person p1 = new Person("Ion", 30);
        Person p2 = new Person("Mihai", 17);
        Person p3 = new Person("Alin", 60);

        personList.add(p1);
        personList.add(p2);
        personList.add(p3);

        personList
                .stream()
                .filter((person) -> person.getAge() < 18)
                .forEach((person) -> System.out.println(person.getName() + " - " + person.getAge()));

        List<String> namePersonList = personList.stream()
                .map((person) -> person.getName())
                .collect(Collectors.toList());

        namePersonList.stream()
                .forEach((name ) -> System.out.println(name));


        // anyMatch() => returneaza true sau false daca CEL PUTIN UN element indeplinesc conditia sau conditile
        boolean atLeastOneAdult = personList.stream()
                .anyMatch((person) -> person.getAge() > 18);

        System.out.println("At least one person is adult: " + atLeastOneAdult);

        //reduce()
        String allNameStr = "";
        for(Person person : personList) {
            allNameStr = allNameStr + person.getName() + " ";
//            allNameStr += person.getName() + " ";
        }

        System.out.println("All names: " + allNameStr);

        // allNames => reprezinta variabila care va tine toate numele
        String result = personList.stream()
                .map((person) -> person.getName()) // returneaza un stream de tipul String
                .reduce("", (allNames, personName) ->  allNames + " " + personName);
        System.out.println(result);

        // sorted()
        personList.stream()
                .sorted((person1, person2) -> person2.getName().compareTo(person1.getName())) // sortare descrescatoare dupa nume
                .forEach((person) -> System.out.println(person.getName() + " - " + person.getAge()));

    }
}
