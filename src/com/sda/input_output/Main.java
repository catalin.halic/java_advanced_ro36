package com.sda.input_output;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Ion");
        names.add("Mihai");
        names.add("Ana");

        // Files.write() => creaza un fisier
        // Path.of() => defineste pozitia relativa a fisierului si ii seteaza numele
        // names => continutul fisierului
        try {
            Files.write(Path.of("java_remote_36.txt"), names);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // fiecare linie din fisier, reprezinta un element in lista
            List<String> fileContentList = Files.readAllLines(Path.of("java_remote_36.txt"));

            fileContentList.stream()
                    .forEach((line) -> System.out.println(line));


        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        Person p1 = new Person("User from file", 100);


        try (FileOutputStream fileOutputStream = new FileOutputStream(Path.of("person.txt").toFile());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(p1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Person person = null;
        try (FileInputStream fileInputStream = new FileInputStream(Path.of("person.txt").toFile());
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {

            // transformam obiectul din inputStream intr-un obiect de tipul Person, folosind cast
            person = (Person) objectInputStream.readObject();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if(person != null) {
            System.out.println(person.getName());
            System.out.println(person.getAge());
        }




    }
}
