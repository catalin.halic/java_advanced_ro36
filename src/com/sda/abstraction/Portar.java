package com.sda.abstraction;

public class Portar extends Angajat {


    public Portar(String nume, int varsta, double tarifOra) {
        super(nume, varsta, tarifOra);
    }

    @Override
    public double calculateSalaryBonus() {
        return 10;
    }

    @Override
    public void pontareCheckIn() {

    }

    @Override
    public void pontareCheckOut() {

    }

    @Override
    public void getZiLibera(String date) {

    }
}
