package com.sda.abstraction;

public abstract class Animal {

    private String name;
    private int age;
    private String gen;
    private String rase;
    private double greutate;

    public Animal(String name, int age, String gen, String rase, double greutate) {
        this.name = name;
        this.age = age;
        this.gen = gen;
        this.rase = rase;
        this.greutate = greutate;
    }

    public abstract void yieldVoice();

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getGreutate() {
        return greutate;
    }

    public void setGreutate(double greutate) {
        this.greutate = greutate;
    }
}
