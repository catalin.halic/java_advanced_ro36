package com.sda.abstraction;

public class Math implements FunctionalInterfaceExample {
    @Override
    public int calculateAB(int a, int b) {
        return a + b;
    }
}
