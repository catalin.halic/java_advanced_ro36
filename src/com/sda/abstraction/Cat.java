package com.sda.abstraction;

public class Cat extends Animal {

    public Cat(String name, int age, String gen, String rase, double greutate) {
        super(name, age, gen, rase, greutate);
    }

    @Override
    public void yieldVoice() {
        System.out.println("Cat -> miau miau");
    }

    @Override
    public String toString() {
        String str = super.toString();

        return "SDADA " + str;
    }
}
