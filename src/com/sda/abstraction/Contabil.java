package com.sda.abstraction;

// - in copiii claselor abstracte ESTE OBLIGATORIU sa suprascriem DOAR metodele ABSTRACTE
// - restul metodelor pot sa fie suprascrise ca in cazul mostenirii (la nevoie)
// - daca metodele care nu sunt abstracte nu sunt sunt suprascrise, se foloseste implementarea din parinte
public class Contabil extends Angajat implements FisaPostExportContabil {

    public Contabil(String nume, int varsta, double tarifOra) {
        super(nume, varsta, tarifOra);
    }

    @Override
    public double calculateSalaryBonus() {
        return 0;
    }

    @Override
    public void printFacturi() {

    }

    @Override
    public void printBilant() {

    }

    @Override
    public String getBilant(String date) {
        return null;
    }

    @Override
    public void pontareCheckIn() {

    }

    @Override
    public void pontareCheckOut() {

    }

    @Override
    public void getZiLibera(String date) {

    }
}
