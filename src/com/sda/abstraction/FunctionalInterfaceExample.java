package com.sda.abstraction;

public interface FunctionalInterfaceExample {
    int calculateAB(int a, int b);

    default int sumAB(int a, int b) {
        return a + b;
    }
}
