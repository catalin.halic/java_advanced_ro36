package com.sda.abstraction;
// clasa abstracta
// - o clasa abstracta se recunoaste dupa keywordul "abstract" semnatura
// - clasa abstracta este folosita in cele mai multe cazuri ca fiind o clasa "parinte"
// - clasa abstracta NU se poate instantia, in modul clasic, adica nu putem folosi new Angajat()
// - clasa abstracta se instantiaza cu ajutorul copiilor sau claselor "Anonime"
// - DOAR in clase abstracte putem avea metode ABSTRACTE, adica metode fara bloc de cod

public abstract class Angajat implements FisaPostului {
    private String nume;
    private int varsta;
    private double tarifOra;

    public Angajat(String nume, int varsta, double tarifOra) {
        this.nume = nume;
        this.varsta = varsta;
        this.tarifOra = tarifOra;
    }

    public double calculateSalary(int nrOre) {
        return this.tarifOra * nrOre;
    }

    /**
     * Metoda abstracta, are keywordul abstract in semnatura, si ";" la final
     * Metodele abstracte se pot definii doar in clasele abstracte
     */
    public abstract double calculateSalaryBonus();

    public String getNume() {
        return nume;
    }

    public int getVarsta() {
        return varsta;
    }

    public double getTarifOra() {
        return tarifOra;
    }

}
