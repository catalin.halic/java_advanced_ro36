package com.sda.abstraction;

public class Main {

    public static void main(String[] args) {
        // Implementarea clasei Angajat cu ajutorul clasei Anonime
        Angajat angajat = new Angajat("catalin", 27, 10000) {
            @Override
            public void pontareCheckIn() {

            }

            @Override
            public void pontareCheckOut() {
            }

            @Override
            public void getZiLibera(String date) {

            }

            // proprietati anonime, pentru ca nu sunt definite in clasa Angajat
            String message = "bun venit";
            int test = 34;

            @Override
            public double calculateSalaryBonus() {
                this.printMessage();
                return 1 + 2 + this.test;
            }

            // metoda anonima, pentru ca nu este definita in clasa Angajat
            public void printMessage() {
                System.out.println(this.message);
            }
        };
        angajat.calculateSalaryBonus();

        Angajat angajat2 = new Angajat("Raul", 27, 12) {
            @Override
            public void pontareCheckIn() {

            }

            @Override
            public void pontareCheckOut() {

            }

            @Override
            public void getZiLibera(String date) {

            }

            @Override
            public double calculateSalaryBonus() {
                return 1 + 1;
            }
        };

        Angajat angajat3 = new Contabil("Cata", 27, 10);

        // () - casting, transformarea unui obiect in alt obiect
//        Contabil x = (Contabil) angajat;


        Circle circle = new Circle(5);
        circle.calculateArie();
        circle.calculatePerimetre();
        System.out.println("circle : arie -> " + circle.getArie() + " perimetru -> " + circle.getPerimetru());

        Rectangle rectangle  = new Rectangle(1,2);
        rectangle.calculateArie();
        rectangle.calculatePerimetre();
        System.out.println("rectangele : arie -> " + rectangle.getArie() + " perimetru -> " + rectangle.getPerimetru());

        int sum1 = calculateAB((a, b) -> a * b);
        int sum2 = calculateAB(new FunctionalInterfaceExample() {
            @Override
            public int calculateAB(int a, int b) {
                return a + b;
            }
        });

        int sum3 = calculateAB(new FunctionalInterfaceExample() {
            @Override
            public int calculateAB(int a, int b) {
                return a - b;
            }
        });
        System.out.println("Sum1 : " + sum1 + " Sum2 : " + sum2);
        System.out.println("Sum3 : " + sum3);
        Math math = new Math();
        System.out.println(math.calculateAB(2, 3));
        Math math1 = new Math();
        System.out.println(math1.calculateAB(2,3));
    }

    public static void testFunctionalInterface() {
        FunctionalInterfaceExample fanctionalInterfaceExample = new FunctionalInterfaceExample() {
            @Override
            public int calculateAB(int a, int b) {
                return a + b;
            }
        };

        fanctionalInterfaceExample.sumAB(2, 3);
//        Lambda Expration
        FunctionalInterfaceExample fanctionalInterfaceExample1 = (int mere, int pere) -> mere + pere;
        FunctionalInterfaceExample fanctionalInterfaceExample2 = (a, b) -> a + b;
    }

    public static int calculateAB(FunctionalInterfaceExample example) {
        return example.calculateAB(2, 3);
    }


}
