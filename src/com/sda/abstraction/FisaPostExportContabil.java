package com.sda.abstraction;

public interface FisaPostExportContabil {

    void printFacturi();

    void printBilant();

    String getBilant(String date);
}
