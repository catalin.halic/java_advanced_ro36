package com.sda.abstraction;

/**
 * - in interfata nu avem proprietati, cu exceptia celor care sunt statice si final adica constante
 * - nu avem constructor
 * - toate metodele din interfete sunt prin definitie abstracte
 * - toate metodele din interfata sunt PUBLICE
 * - nu putem instantia direct o interfata
 * - pentru instantiere folosim interfete anonime sau Clase care implementeaza acele interfete
 * - o interfata se implementeaza intr-o clasa cu ajutorul lui "implements"
 * - o clasa poate sa implementeze o INFINITATE de interfete (citeste despre SOLID Principles)
 * - keyword "default" ne permite sa avem metode cu block (secventa) de cod in interfete
 * - a NU se confunda cu modificatorul de acces "default"
 * - metodele care sunt default NU trebuie sa le implementam, dar putem sa le suprascriem
 * - default a fost introdus de la java 8 incoace
 */
public interface FisaPostului {
    void pontareCheckIn();

    void pontareCheckOut();

    void getZiLibera(String date);

    default void printMessage(String message) {
        System.out.println("Inferfata " + message);
    }

}

/**
 * Interfete functionale
 * - o interfata functionala este o interfata care contine o singura metoda ABSTRACTA
 * - ea poate sa contina o infinitate de metode default
 */
