package com.sda.abstraction;

public class Dog extends Animal {

    public Dog(String name, int age, String gen, String rase, double greutate) {
        super(name, age, gen, rase, greutate);
    }

    @Override
    public void yieldVoice() {
        System.out.println("Dog -> haw haw");
    }
}
