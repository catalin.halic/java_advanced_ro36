package com.sda.optionals;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        String name = "Popescu";

//        Optional<String> optionalName = Optional.of(name); // valoare nu are voie sa fie null, ne va arunca o exceptie
        Optional<String> optionalNameNullable = Optional.ofNullable(name); // valoarea poate sa fie null, se asteapta sa fie null

        if(optionalNameNullable.isPresent()) {
            System.out.println("Numele este diferit de null");
            String value = optionalNameNullable.get(); // luam valoarea dintr-un optional
            System.out.println(value);
        } else {
            System.out.println("Numele este null");
        }

        if(name != null) {
            System.out.println("nume diferit de null");
        } else {
            System.out.println("nume este null");
        }

        optionalNameNullable.ifPresent((String value) -> System.out.println("Value este prezent: " + value));
        optionalNameNullable.ifPresentOrElse((String value) -> System.out.println("Value este prezent"), () -> System.out.println("nu este prezent"));
        optionalNameNullable.orElseThrow(); // arunca o exceptie

//        orElseThrow()
//        if(name == null) {
//            throw  new Exception();
//        }
    }

}
