package com.sda.enums;

import com.sda.abstraction.Shape;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println(LengthUnit.CM);
        System.out.println(LengthUnit.KM);
        System.out.println(LengthUnit.FOOT);
        System.out.println(LengthUnit.METER);

        LengthUnit unit;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Name of unit:");
        String unitName = scanner.nextLine();

        unit = LengthUnit.valueOf(unitName);

        // asa cumparam un enum
        if(unit == LengthUnit.CM) {
            System.out.println("Acesta este centimetru!");
        }

        switch (unit) {
            case CM:
                System.out.println("Centimetru");
                System.out.println(LengthUnit.CM);
                break;
            case FOOT:
                System.out.println("Picioare");
                break;
            case METER:
                System.out.println("Meter");
                break;
            case KM:
                System.out.println("Kilometru");
                break;
            default:
                System.out.println("Cazul default");
        }



        System.out.println("--------");

        for(LengthUnit lengthUnit : LengthUnit.values()) {
            System.out.println(lengthUnit + " convert to meter: " + lengthUnit.convertToMeter(20, lengthUnit) );
            System.out.println("-----");
        }

        // accesarea unei valori din enum
        System.out.println(LengthUnit.CM.value);

        System.out.println("------ ");
        System.out.println(LengthUnit.valueOf("KM").value);
    }
}
