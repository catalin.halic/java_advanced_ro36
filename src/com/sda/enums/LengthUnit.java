package com.sda.enums;

 public enum LengthUnit {
     // instantierea enumurilor
    CM,
    METER,
    FOOT,
    KM;

     double value;

     // constructor
     LengthUnit() {
         this.value = value;
     }

     // TODO: conversia intre unitatile de masura.
     double convertToMeter(double value, LengthUnit unit) {
         switch (unit) {
             case CM: {
                 return value * 0.001;
             }
             case FOOT: {
                 return value * 0.332;
             }
             case METER: {
                 return value * 1;
             }
             case KM: {
                 return value * 1000;
             }
             default: {
                 return -1;
             }


         }

     }

     @Override
     public String toString() {
         return this.name();
     }
 }
