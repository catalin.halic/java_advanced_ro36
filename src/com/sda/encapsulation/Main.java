package com.sda.encapsulation;
//numele clasei com.sda.encapsulation.Main;

public class Main {
    public static void main(String[] args) {
        Person p = new Person("cata");

//        p.name = "           ";
        p.setName("      d   ");

//        p.age = -1;
//        p.setAge(§;

        System.out.println(p.getName());

        Pocket pocket = new Pocket(-5);
        System.out.println(pocket.getMoney());

        pocket.setMoney(-3);
        pocket.setMoney(4000);

        System.out.println(pocket.getMoney());
    }
}
